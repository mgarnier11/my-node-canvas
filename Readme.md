Package initially coming from : https://github.com/Automattic/node-canvas all credits are due to the original authors.

Feel free to use it, but considering i only made the changes for my personnal use only, i wont be updating this package if anything new comes to the original package. (node-canvas version : v2.11.2)

How to build

- Build the dockerfile
- Run the dockerfile and add a bind mount to the repo folder
- Enter the container created and install node 18.11.0
- Go to the mounted folder and run these commands :

```bash
npm install -g node-gyp
npm install --ignore-scripts
apt-get install pax-utils
cp prebuild/Linux/binding.gyp binding.gyp
node-gyp rebuild -j 2
. prebuild/Linux/bundle.sh
. prebuild/tarball.sh
```

- You will now have the prebuild binaries in the canvas.tar.gz file
